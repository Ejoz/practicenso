import time
import random
from threading import Thread

from config import PRACTICE_CONF, DEBUG, SECOND_DURATION, PENALTY_DURATION, PENALTIES, NB_MAX_PENALTIES, \
    lock_pbt, COMMENTS_B, write_terminal, whistle
from derby.player.player import Player
from derby.rules.penalty_box_timer import PenaltyBoxTimer


class BlockerSimulator(Thread):

    """ Player Simulator """

    def __init__(self, player: Player, pbt: PenaltyBoxTimer, practice=PRACTICE_CONF):

        Thread.__init__(self)

        self.practice = practice
        self.player = player

        self.track = False    # Player is on the track

        self.pbt = pbt  # Penalty Box Timer
        self.id_line_sheet = -1     # Line of current Penalty
        self.id_sej = 1     # Sej Interjam

    def end_jam(self):
        self.track = False

    def get_name(self):
        return self.player.get_name()

    def write_action(self, msg: str, reverse=False):
        write_terminal(self.player.get_name() + '[' + self.player.get_jamPosition()
                             + ' #' + self.player.get_number() + '] '
                             + msg, self.player.get_color(), reverse)

    def write_comment(self, msg: str):

        write_terminal(self.player.get_name() + ' ' + msg , self.player.get_color())

    def blocker_waiting(self, t):

        """  Wait in penalty box """

        time_done = 0
        while time_done < t and self.track:
            time.sleep(SECOND_DURATION)
            time_done = time_done + 1

        return time_done

    def end_jam_in_pbt(self, t_in_pb, time_done):

        # Time to stay
        time_to_stay = t_in_pb - time_done
        self.player.stay_in_pb(time_to_stay)

        # Modifies the PBT
        with lock_pbt:
            self.pbt.add_end_jam(self.id_line_sheet, self.id_sej)

        # Increments id SEJ
        self.id_sej += 1

    def reset_pbt_line(self):

        self.id_line_sheet = -1  # Line number on the sheet
        self.id_sej = 1  # Reset SEJ id

    def liberate_blocker(self):

        # Notify the PBT
        with lock_pbt:
            self.pbt.liberate_player_in_pb()

        # The PBT line is closed
        self.reset_pbt_line()

        # Notify the Player
        self.player.out_penaltyBox()

    def out_pbt(self, t_start):

        # Message end waiting
        msg = 'sort de PB'
        if DEBUG:
            t_2 = time.time() - t_start
            msg += ' au bout de ' + str(t_2)[:3] + 's.'

        self.write_action(msg)

    def wait_in_box(self, t_in_pb):
        self.blocker_wait_in_box(t_in_pb)

    def blocker_wait_in_box(self, t_in_pb):

        # Start time
        t_1 = time.time()

        # Wait
        time_done = self.blocker_waiting(t_in_pb)

        # Player has to stay in PB after the jam ending
        if not self.track:
            self.end_jam_in_pbt(t_in_pb, time_done)

        # End penalty waiting
        else:
            self.out_pbt(t_1)
            self.liberate_blocker()

    def make_penalty(self):

        def rand_penalty():
            id_penalty = random.randint(0, len(PENALTIES) - 1)
            penalty = PENALTIES[id_penalty]
            return penalty

        nb_penalty = max(1, random.randint(0, NB_MAX_PENALTIES)) # P(make 1 penalty) = 2/3
        l_penalty = list()

        # The penalty is randomy choosen
        penalty = rand_penalty()
        l_penalty.append(penalty)
        msg_description = penalty.get_description()

        k = nb_penalty - 1
        while k > 0:
            msg_description += ' ET '
            penalty = rand_penalty()
            l_penalty.append(penalty)
            msg_description += penalty.get_description()
            k = k - 1

        # Describe the penalty
        self.write_action(msg_description)
        whistle('TWIIIIIT')

        # Player go to the box
        time.sleep(SECOND_DURATION * 3)
        self.write_action("s'assoit en box", self.practice['PBT'])
        self.player.in_penaltyBox()

        with lock_pbt:
            btj = ''
            if not self.track:  # Player arrives between Jam
                btj = 'X'

            # Add Player on the PBT sheet
            self.id_line_sheet = self.pbt.new_player_in_pb(self.player, btj, 1)

        return nb_penalty

    def run(self):

        # Start Jam
        self.track = True

        if DEBUG:
            self.write_action('start')

        # If the payer is in PB, stay in PB
        if self.player.is_in_penaltyBox():
            self.wait_in_box(self.player.get_interJam())

        # Wait for the next action
        s_action = random.uniform(7, 10)
        time.sleep(SECOND_DURATION * s_action)

        # Go to the track
        while self.track:

            if self.pbt.get_nb_pb() < 2 and random.random() < self.player.get_proba_penalty(): # TODO: ecrire la penalite sur une feuille
                nb_penalty = self.make_penalty()  # Receive one or two penalties
                self.wait_in_box(PENALTY_DURATION * nb_penalty)  # Player is waiting in box

            else:
                id_comment = random.randint(0, len(COMMENTS_B) - 1)
                self.write_comment(COMMENTS_B[id_comment])

            # Wait for the next action
            t_wait = random.uniform(7, 10)
            time.sleep(SECOND_DURATION * t_wait)

        return False



