import time
from threading import Thread

from config import DEBUG, JAM_MAX_DURATION, SECOND_DURATION, write_terminal
from derby.object.timer import Timer


class JamTimerSimulator(Thread):

    """ Class of JamTimerSimulator """

    def __init__(self, timer: Timer):

        Thread.__init__(self)

        self.timer = timer
        self.timer.reset()

        self.running = False
        self.t_start = 0

    def write_time(self):

        msg = 'JamTimer: ' + str(self.timer.get_time())
        if DEBUG:
            msg += ' (real: ' + str(time.time() - self.t_start) + ')'

        write_terminal(msg, 'grey', False)

    def run(self):

        self.t_start = time.time()
        self.running = True

        t = 0
        while self.running and t < JAM_MAX_DURATION:
            time.sleep(SECOND_DURATION)
            t += 1
            self.timer.add_time(1)

            if DEBUG:
                self.write_time()

        self.stop()     # End Jam

    def stop(self):

        """ End of the jam """

        self.running = False
