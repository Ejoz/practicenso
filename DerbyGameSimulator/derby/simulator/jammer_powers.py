import random
import time

from config import lock_pbm
from .blocker import DEBUG, BlockerSimulator, Player, PRACTICE_CONF, lock_pbt, SECOND_DURATION
from derby.rules.scorekeeper import ScoreKeeper
from derby.rules.penalty_box_manager import PenaltyBoxManager


class JammerPowers(BlockerSimulator):

    """ Jammer Actions (score, penalty box) """

    def __init__(self, player: Player, pbm: PenaltyBoxManager, team: int,
                 sk: ScoreKeeper, practice: dict = PRACTICE_CONF):

        pbt = pbm.get_pbt(team)
        BlockerSimulator.__init__(self, player, pbt, practice)

        self.sk = sk  # ScoreKeeper
        self.pbm = pbm  # Penalty Box Manager
        self.team = team  # Team player

    def score(self):

        """ Jammer scores points """

        points = random.randint(2, 4)
        msg = 'marque ' + str(points) + ' points'

        self.write_action(msg, self.practice['SK'])

        self.sk.add_score(points)

    def jammer_waiting(self, t):

        """  Wait in penalty box """

        time_done = 0
        while time_done < t and self.track and self.pbm.is_in_penalty_box(self.team):
            time.sleep(SECOND_DURATION)
            time_done = time_done + 1

        return time_done

    def liberate_jammer(self):

        # Notify the PBT
        with lock_pbt:
            self.pbt.jammer_liberate_jammer(self.id_line_sheet)

        # The line is closed
        self.reset_pbt_line()

        # Notify the Player
        self.player.out_penaltyBox()

    def jammer_wait_in_box(self, t_in_pb):

        # Start time
        t_1 = time.time()

        # Notifies the PBM
        with lock_pbm:
            self.pbm.notify_pbm(self.team)

            # Wait ...
            time_done = self.jammer_waiting(t_in_pb)

        # Player has to stay in PB after the jam ending
        if not self.track:
            self.end_jam_in_pbt(t_in_pb, time_done)

        # End waiting : time is done
        elif time_done >= t_in_pb:
            self.out_pbt(t_1)
            self.liberate_blocker()

        # End waiting : other jammer arrives
        else:
            self.out_pbt(t_1)
            self.liberate_jammer()


