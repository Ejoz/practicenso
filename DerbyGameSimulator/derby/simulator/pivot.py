import random
import time

from config import SECOND_DURATION, PENALTIES, PENALTY_DURATION, COMMENTS_B, COMMENTS_J, JAMMER_STR, lock_pbm
from .jammer_powers import PenaltyBoxManager, DEBUG, PRACTICE_CONF, Player, JammerPowers, ScoreKeeper


class PivotSimulator(JammerPowers):

    def __init__(self, player: Player, pbm: PenaltyBoxManager, team: int, sk: ScoreKeeper,
                 practice: dict = PRACTICE_CONF):

        JammerPowers.__init__(self, player, pbm, team, sk, practice)
        self.jammer_cover = False   # Jammer cover

    def is_in_box(self):
        return self.player.is_in_penaltyBox()

    def set_jammer(self, initial_pass: bool):

        msg = 'devient jammer'
        self.write_action(msg, self.practice['SK'])

        # Notifies the scorekeeper
        self.sk.star_pass(self.player.get_number())

        self.player.set_jam_position(JAMMER_STR)
        self.jammer_cover = True

        if not initial_pass:
            if self.track:
                msg = 'termine le passage initial'
            else:
                msg = 'ne termine pas le passage initial'
            self.write_action(msg, self.practice['SK'])

    def wait_in_box(self, t_in_pb):

        if self.jammer_cover:
            self.jammer_wait_in_box(t_in_pb)
        else:
            self.blocker_wait_in_box(t_in_pb)

    def run(self):

        # Start Jam
        self.track = True

        if DEBUG:
            self.write_action('start')

        # If the payer is in PB, stay in PB
        if self.player.is_in_penaltyBox():
            self.wait_in_box(self.player.get_interJam())

        # Wait for the next action
        s_action = random.uniform(7, 10)
        time.sleep(SECOND_DURATION * s_action)

        # Go to the track
        while self.track:

            if self.pbt.get_nb_pb() < 2 and random.random() < self.player.get_proba_penalty():
                nb_penalty = self.make_penalty()  # Receive one or two penalties
                self.wait_in_box(PENALTY_DURATION * nb_penalty)  # Player is waiting in box

            elif self.jammer_cover:

                if random.random() < 0.5:
                    self.score()
                else:
                    id_comment = random.randint(0, len(COMMENTS_J) - 1)
                    self.write_comment(COMMENTS_J[id_comment])

            else:
                id_comment = random.randint(0, len(COMMENTS_B) - 1)
                self.write_comment(COMMENTS_B[id_comment])

            # Wait for the next action
            t_wait = random.uniform(7, 10)
            time.sleep(SECOND_DURATION * t_wait)

        return False

