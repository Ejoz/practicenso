import random
import time

from config import SECOND_DURATION, PENALTY_DURATION, BLOCKER_STR, PENALTIES, COMMENTS_J, COMMENTS_B, whistle, lock_pbm

from .jammer_powers import JammerPowers, DEBUG, PRACTICE_CONF, Player, PenaltyBoxManager, ScoreKeeper
from .pivot import PivotSimulator


class JammerSimulator(JammerPowers):

    def __init__(self, player: Player, pbm: PenaltyBoxManager, team: int, lead: bool,
                 sk: ScoreKeeper, pivot_sim: PivotSimulator, practice: dict = PRACTICE_CONF):

        JammerPowers.__init__(self, player, pbm, team, sk, practice)

        self.jammer_cover = True   # Jammer cover
        self.lead = lead  # Lead
        self.pivot_sim = pivot_sim  # Pivot

    def is_blocker(self):
        return not self.jammer_cover

    def star_pass(self, initial_pass):

        """ Jammer do start pass """

        msg = 'effectue un star pass'
        self.write_action(msg, self.practice['SK'])

        self.jammer_cover = False  # Lost the cover
        self.lead = False   # The lead is lost
        self.player.set_jam_position(BLOCKER_STR)   # Notifies the player
        self.pivot_sim.set_jammer(initial_pass)     # Notifies the pivot

    def wait_in_box(self, t_in_pb):

        if self.jammer_cover:
            self.jammer_wait_in_box(t_in_pb)
        else:
            self.blocker_wait_in_box(t_in_pb)

    def run(self):

        # Start Jam
        self.track = True

        if DEBUG:
            self.write_action('start')

        # If the payer is in PB, stay in PB
        if self.player.is_in_penaltyBox():
            self.wait_in_box(self.player.get_interJam())

        # Wait for the initial pass
        s_j = 0
        if not self.lead:
            s_j = 4

        s_action = random.uniform(2 + s_j, 5 + s_j)
        time.sleep(SECOND_DURATION * s_action)

        if self.track:
            if (not self.pivot_sim.is_in_box()) and random.random() < 0.25:
                self.star_pass(False)
            else:
                self.write_action('termine son passage initial', self.practice['SK'])
        else:
            self.write_action('ne termine pas son passage initial', self.practice['SK'])
            self.sk.no_initial()
            return False

        # Lead Jammer
        if self.lead:
            whistle('TWIT TWIT')
            self.write_action('obtient le lead', self.practice['SK'])
            self.sk.lead()
            time.sleep(SECOND_DURATION * 5)

        # Go to the track
        while self.track:

            # Do a penalty
            if random.random() < self.player.get_proba_penalty():
                nb_penalty = self.make_penalty()  # Receive one or two penalties
                self.wait_in_box(PENALTY_DURATION * nb_penalty)  # Player is waiting in box

                # Loose the lead
                if self.lead:
                    self.lead = False
                    self.sk.lost()  # Notifies the scorekeeper

            # Stop the jam
            elif self.lead and random.random() < 0.25:
                self.write_action('met fin au Jam', self.practice['SK'])
                self.sk.call()
                return True

            # The jammer has is cover
            elif self.jammer_cover:

                # Score points
                if random.random() < 0.5:
                    self.score()

                # Star Pass
                elif (not self.pivot_sim.is_in_box()) and random.random() < 0.25:
                    self.star_pass(True)

                # Comment
                else:
                    id_comment = random.randint(0, len(COMMENTS_J) - 1)
                    self.write_comment(COMMENTS_J[id_comment])

            # Player is a Blocker
            else:
                id_comment = random.randint(0, len(COMMENTS_B) - 1)
                self.write_comment(COMMENTS_B[id_comment])

            # Wait for the next action
            s_action = random.uniform(7, 10)
            time.sleep(SECOND_DURATION * s_action)

        return False
