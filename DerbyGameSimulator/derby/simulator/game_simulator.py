import random
import time

from config import DEBUG, PRACTICE_CONF, SECOND_DURATION, COLOR_TEAM_0, COLOR_TEAM_1, \
    INTERJAM_DURATION, JAM_TIMER, write_terminal, whistle

from derby.simulator.jamtimer import JamTimerSimulator
from derby.simulator.blocker import BlockerSimulator
from derby.simulator.jammer import JammerSimulator
from derby.simulator.pivot import PivotSimulator

from derby.rules.penalty_box_timer import PenaltyBoxTimer
from derby.rules.penalty_box_manager import PenaltyBoxManager
from derby.rules.scorekeeper import ScoreKeeper

from derby.player.team import Team

# Penalty Box
PBT_0 = PenaltyBoxTimer(COLOR_TEAM_0, 'PBT 0')
PBT_1 = PenaltyBoxTimer(COLOR_TEAM_1, 'PBT 1')
PBM = PenaltyBoxManager(PBT_0, PBT_1)


# Scorekeeper
SK_0 = ScoreKeeper(COLOR_TEAM_0, 'SK 0')
SK_1 = ScoreKeeper(COLOR_TEAM_1, 'SK 1')


class Jam(object):

    def __init__(self, l_players_0, l_players_1, practice=PRACTICE_CONF):

        # Practice NSO
        self.practice = practice

        # Jam players
        self.players_0 = l_players_0
        self.players_1 = l_players_1

        # Choose the team who will have the lead
        id_team_lead = random.randint(0, 1)
        self.lead_jammer = [False, False]
        self.lead_jammer[id_team_lead] = True

        # Jam duration
        self.duration = 0

    def get_duration(self):
        return self.duration

    def start(self):

        # ==============================================================================================================
        #                                          Align Players
        # ==============================================================================================================

        # Construction of Player Simulator
        l_player_sim = list()  # List of player simulator
        for k in range(2):
            if k == 0:
                pbt = PBT_0
                sk = SK_0
                l_players = self.players_0
            else:
                pbt = PBT_1
                sk = SK_1
                l_players = self.players_1

            # Pivot
            pivot = l_players[-1]
            pivot_sim = PivotSimulator(pivot, PBM, k, sk, self.practice)
            l_player_sim.append(pivot_sim)

            # Jammer
            jammer = l_players[0]
            sk.set_jammer(jammer.get_number())  # Notifies SK

            jammer_sim = JammerSimulator(jammer, PBM, k, self.lead_jammer[k], sk, pivot_sim, self.practice)
            l_player_sim.append(jammer_sim)

            if self.lead_jammer[k]:
                lead_jammer = jammer_sim

            # Blocker
            l_player_blocker = l_players[1:-1]
            for player in l_player_blocker:
                l_player_sim.append(BlockerSimulator(player, pbt, self.practice))

        # JamTimer simulator
        jam_timer_sim = JamTimerSimulator(JAM_TIMER)

        # 5 SECONDS
        t_start = time.time()
        whistle('5 SECONDS')
        time.sleep(5 * SECOND_DURATION)

        # ==============================================================================================================
        #                                          Jam start
        # ==============================================================================================================
        whistle('TWIT')

        # Start JamTimer
        jam_timer_sim.start()

        # ==============================================================================================================
        #                                           Jam is runnning
        # ==============================================================================================================

        # Start players
        for player_sim in l_player_sim:
            player_sim.start()

        # JamTimer is running and Jamleader does not stop jam
        while jam_timer_sim.is_alive() and lead_jammer.is_alive():
            pass

        # ==============================================================================================================
        #                                           End Jam
        # ==============================================================================================================
        whistle('TWIT TWIT TWIT TWIT')
        if DEBUG:
            t_stop = time.time() - t_start
            write_terminal('Time :' + str(t_stop))

        # Stop JamTimer
        jam_timer_sim.stop()

        # Stop players
        for player_sim in l_player_sim:
            player_sim.end_jam()

        whistle('TWIT TWIT TWIT TWIT')
        whistle('TWIT TWIT TWIT TWIT')

        t_stop = time.time() - t_start
        self.duration = t_stop

        # Wait players stops and jamtimer
        jam_timer_sim.join()
        for player_sim in l_player_sim:
            player_sim.join()

        # SK write star pass adv.
        jammer_sim_0 = l_player_sim[1]
        if jammer_sim_0.is_blocker():
            SK_1.star_pass_adv()

        jammer_sim_1 = l_player_sim[6]
        if jammer_sim_1.is_blocker():
            SK_0.star_pass_adv()

        if DEBUG:
            t_stop = time.time() - t_start
            write_terminal('Jam real duration :' + str(t_stop))


def game_simulator(file_team_0, file_team_1, rt: int, practice: dict, name_0='TEAM 0', name_1='TEAM 1'):

    """ Game simulator """

    def print_player_in_pb(team):

        player_in_pb = team.players_in_pb()
        write_terminal('TEAM ' + team.get_name(), team.get_color(), True)
        for player in player_in_pb:
            write_terminal(str(player), 'grey', True)

    # ===================================================================================================
    #                                            DEFINE TEAMS
    # ===================================================================================================
    team_0 = Team(name_0, COLOR_TEAM_0)
    team_0.import_players(file_team_0)

    team_1 = Team(name_1, COLOR_TEAM_1)
    team_1.import_players(file_team_1)

    l_player_0_track = list()
    l_player_1_track = list()

    # ====================================================================================================
    #                                           START GAME
    # ====================================================================================================
    id_jam = 1
    remaining_time = rt
    while remaining_time > 0:

        # Bench liners align players
        l_player_0_track = team_0.bench_liner(l_player_0_track)
        l_player_1_track = team_1.bench_liner(l_player_1_track)

        # Write players in PB
        if DEBUG:
            print_player_in_pb(team_0)
            print_player_in_pb(team_1)

        jam = Jam(l_player_0_track, l_player_1_track, practice)
        msg = ' Jam ' + str(id_jam) + '\t'
        write_terminal(msg, 'grey', True)

        # Start Jam
        jam.start()

        # Reamaining period time
        jam_duration = int(jam.get_duration())
        remaining_time -= jam_duration

        # Inter Jam
        if remaining_time > 0:
            write_terminal('INTERJAM', 'grey', True)
            time.sleep(SECOND_DURATION * INTERJAM_DURATION)
            remaining_time -= INTERJAM_DURATION

        # Reset for the next jam
        id_jam += 1
        PBT_0.new_jam(jam_duration)
        PBT_1.new_jam(jam_duration)
        SK_0.new_jam()
        SK_1.new_jam()

    write_terminal('END GAME', 'grey', True)

    # PBT write sheets
    PBT_0.print()
    PBT_1.print()

    # SK write sheets
    SK_0.print()
    SK_1.print()


