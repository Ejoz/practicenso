import pandas as pd
import random
import copy

from config import TRACK_POSITION
from derby.player.player import Player


class Team(object):

    def __init__(self, name: str, color: str):

        self.name = name
        self.color = color
        self.l_players = list()

    def get_color(self):
        return self.color

    def get_name(self):
        return self.name

    def import_players(self, team_file):

        # Import number and name
        df_players = pd.read_csv(team_file)

        # Create players list
        for p in df_players.itertuples():
            player = Player(str(p[1]), str(p[2]), self.color)
            self.l_players.append(player)

        # Sort player using number
        self.l_players.sort(key=lambda p: p.get_number())

    def players_in_pb(self, l_player_jam=list()):

        if len(l_player_jam) == 0:
            l_player = self.l_players
        else:
            l_player = l_player_jam

        return [player for player in l_player if player.get_interJam() > 0 and player.is_in_game()]

    def bench_liner(self, l_player_jam: list):

        """ Returns the next line for the nex jam """

        def choose_free_player(l_all_players, nb):

            l_free = [player for player in l_all_players if player.get_interJam() <= 0 and player.is_in_game()]

            if nb >= len(l_free):
                return l_free

            return random.sample(l_free, nb)

        # Select player in PB that are still in the game - TODO : Full out
        l_player_next = [player for player in l_player_jam if player.get_interJam() > 0 and player.is_in_game()]

        d_position = copy.deepcopy(TRACK_POSITION)
        for player in l_player_next:
            d_position[player.get_jamPosition()] = d_position[player.get_jamPosition()] - 1

        # Select player that are not in PB and still in the game
        i = 0
        random_player = choose_free_player(self.l_players, 5)
        for role in d_position:
            nb_role = d_position[role]
            for k in range(nb_role):
                player = random_player[i]
                player.set_jam_position(role)
                l_player_next.append(player)
                i += 1

        # Selection for the next jam order : Jammer, Blocker, Pivot
        l_player_next.sort(reverse=True)

        return l_player_next

    def __str__(self):

        aff = 'Team ' + self.color + ': ' + self.name
        for player in self.l_players:
            aff += '\n ' + str(player)
        return aff













