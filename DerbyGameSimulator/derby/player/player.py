import random

from config import NB_PENALTIES_FULL_OUT, JAMMER_STR, PIVOT_STR


class Player(object):

    def __init__(self, number: str, name: str, color: str):

        # Static for during the game
        self.name = name
        self.number = str(number)
        self.color = color
        self.proba_penalty = random.uniform(0.05, 0.17)     # Probability to do a penalty

        # Change during the game
        self.in_game = True
        self.nb_penalty = 0

        # Change for all jam
        self.jam_position = ''
        self.jam_selected = False

        # Penalty Box
        self.time_in_pb = -1
        self.is_in_pb = False

    def __str__(self):
        return '#' + self.number + ' \t' + self.name

    def __repr__(self):
        return self.name

    def __lt__(self, other):

        if self.jam_position == JAMMER_STR:
            return False

        if self.jam_position == PIVOT_STR:
            return True

        if other.jam_position == JAMMER_STR:
            return True

        return self.number < other.number

    def __eq__(self, other):
        return self.number == other.number and self.color == other.color

    def is_in_penaltyBox(self):
        return self.is_in_pb

    def is_in_game(self):
        return self.in_game

    def is_jammer(self):
        return self.jam_position == JAMMER_STR

    def is_pivot(self):
        return self.jam_position == PIVOT_STR

    def get_name(self):
        return self.name

    def get_number(self):
        return self.number

    def get_color(self):
        return self.color

    def get_ref_called(self, msg: str = ''):
        return self.color + ' #' + self.number + ' ' + msg

    def get_proba_penalty(self):
        return self.proba_penalty

    def get_jamPosition(self):
        return self.jam_position

    def get_interJam(self):
        return self.time_in_pb

    def set_jam_position(self, p: str):
        self.jam_position = p

    def stay_in_pb(self, t: int):
        self.time_in_pb = t

    def add_penalty(self, nb=1):
        self.nb_penalty += 1
        if self.nb_penalty >= NB_PENALTIES_FULL_OUT: # Full out
            self.in_game = False

    def out_penaltyBox(self):
        self.time_in_pb = -1
        self.is_in_pb = False

    def in_penaltyBox(self):
        self.is_in_pb = True



