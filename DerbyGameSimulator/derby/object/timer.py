class Timer(object):

    """ Timer class """

    def __init__(self, max_time):
        self.max_time = max_time
        self.time_elapsed = 0

    def reset(self):
        self.time_elapsed = 0

    def add_time(self, s):
        self.time_elapsed += s

    def get_time(self):
        return self.time_elapsed

    def get_max_time(self):
        return self.max_time
