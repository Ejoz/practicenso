import pandas as pd
import copy

from .nso import NSO, CROSS


COLUMN_ID = dict({'JAM': 0, '# Jammer': 1,
                'LOST': 2, 'LEAD': 3, 'CALL': 4,
                'INJ.': 5, 'IN': 6, 'TRIP_2': 7,
                'TRIP_3': 8, 'TRIP_4': 9, 'TRIP_5': 10,
                'TRIP_6': 11, 'TRIP_7': 12, 'TRIP_8': 13,
                'TRIP_9': 14, 'TRIP_10': 15, 'Jam Total': 16,
                'Total': 17})


class ScoreKeeper(NSO):

    def __init__(self, color_team: str, name_nso: str):

        NSO.__init__(self, color_team, name_nso)

        self.col = list(COLUMN_ID.keys())

        # Initialize SK Sheet
        for label in self.col:
            self.sheet[label] = []

        # Empty line
        self.empty_line = ['' for label in COLUMN_ID]
        for k in range(COLUMN_ID['TRIP_2'], COLUMN_ID['TRIP_10'] + 1):
            self.empty_line[k] = 0

        self.line_jammer = list()   # Jammer line
        self.line_pivot = list()    # SP line
        self.line_sp_adv = list()   # SP* line
        self.reset_lines()

        self.sp = False
        self.sp_adv = False

        self.trip = 2

    def reset_lines(self):

        self.line_jammer = copy.deepcopy(self.empty_line)
        self.line_pivot = copy.deepcopy(self.empty_line)
        self.line_sp_adv = copy.deepcopy(self.empty_line)

        self.sp = False
        self.sp_adv = False

        self.trip = 2

    def add_score(self, score):

        id_label = 'TRIP_' + str(self.trip)
        if self.sp:
            self.line_pivot[COLUMN_ID[id_label]] = score
        else:
            self.line_jammer[COLUMN_ID[id_label]] = score

        self.trip += 1

    def team_star_pass(self):
        self.sp = True

    def adv_star_pass(self):
        self.sp_adv = True

    def add_lines(self):

        # Add Jammer line
        self.line_jammer[COLUMN_ID['JAM']] = self.jam

        row = pd.DataFrame([self.line_jammer], columns=self.col, index=[self.line_number])
        self.sheet = self.sheet.append(row)
        self.line_number += 1

        # If a star pass happen in this team
        if self.sp:
            row = pd.DataFrame([self.line_pivot], columns=self.col, index=[self.line_number])
            self.sheet = self.sheet.append(row)
            self.line_number += 1

        # If a star pass happen in the other team
        if self.sp_adv:
            row = pd.DataFrame([self.line_sp_adv], columns=self.col, index=[self.line_number])
            self.sheet = self.sheet.append(row)
            self.line_number += 1

        self.reset_lines()

    def new_jam(self):
        self.add_lines()
        self.next_jam()

    def set_jammer(self, id_jammer):
        self.line_jammer[COLUMN_ID['# Jammer']] = id_jammer

    def lead(self):
        self.line_jammer[COLUMN_ID['LEAD']] = CROSS

    def lost(self):
        self.line_jammer[COLUMN_ID['LOST']] = CROSS

    def call(self):
        self.line_jammer[COLUMN_ID['CALL']] = CROSS

    def no_initial(self):
        self.line_jammer[COLUMN_ID['IN']] = CROSS

    def star_pass(self, id_pivot):

        self.line_pivot[COLUMN_ID['JAM']] = 'SP'
        self.line_pivot[COLUMN_ID['# Jammer']] = id_pivot

        self.sp = True

    def star_pass_adv(self):

        self.line_sp_adv[COLUMN_ID['JAM']] = 'SP*'

        self.sp_adv = True

    def print(self):

        def total_jam(line):

            jam_sum = 0
            for k in range(COLUMN_ID['TRIP_2'], COLUMN_ID['TRIP_10'] + 1):
                jam_sum += line[k]
            return jam_sum

        self.sheet['Jam Total'] = self.sheet.apply(total_jam, axis=1)
        self.sheet['Total'] = self.sheet['Jam Total'].cumsum()

        self.print_sheet('SK')


