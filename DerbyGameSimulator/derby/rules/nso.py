import pandas as pd

CROSS = 'X'


class NSO(object):

    def __init__(self, color_team: str, name_nso: str):

        self.name_nso = name_nso
        self.team = color_team[0]

        # Sheet
        self.sheet = pd.DataFrame({})
        self.line_number = 0

        # Game
        self.period = 1
        self.jam = 1

    def __len__(self):
        return len(self.sheet)

    def __str__(self):
        return str(self.sheet)

    def new_period(self):
        self.period += 1

    def reset_period(self):
        self.period = 1
        self.jam = 1

    def next_jam(self):
        self.jam += 1

    def print_sheet(self, role):

        """ Print the sheet """

        self.sheet.to_csv(role + '_' + self.team + '.csv', index=False)

