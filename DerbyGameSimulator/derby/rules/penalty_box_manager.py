from .penalty_box_timer import PenaltyBoxTimer, Player
from config import JAMMER_STR


class PenaltyBoxManager(object):

    """ Penalty Box Manager """

    def __init__(self, pbt_0: PenaltyBoxTimer, pbt_1: PenaltyBoxTimer):

        self.l_pbt = [pbt_0, pbt_1]
        self.l_jammer = [False, False]

    def notify_pbm(self, team: int):

        """ Add a new player in penalty box """

        if 0 <= team <= 1:
            other = (team + 1) % 2
            self.l_jammer[team] = True  # The team Jammer is in PBT
            self.l_jammer[other] = False    # Notify the other Jammer is liberate

    def is_in_penalty_box(self, team: int):

        """ Return True if the player is not a jamer or if the Jammer team is still in pbt jammer """

        return self.l_jammer[team]

    def get_pbt(self, team):

        """ Return the team PBT """

        return self.l_pbt[team]





