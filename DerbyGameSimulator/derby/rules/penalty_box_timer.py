import pandas as pd

from config import PENALTY_DURATION, JAM_TIMER
from derby.player.player import Player
from .nso import NSO


class PenaltyBoxTimer(NSO):

    def __init__(self, color_team: str, name_nso: str):

        NSO.__init__(self, color_team, name_nso)

        # Sheet
        self.col = ['Period', 'Jam', 'Btw Jam', 'Team', 'Skater #', 'Pos', 'In', 'Stand', 'Done', 'SEJ_1', 'SEJ_2', 'SEJ_3']
        self.sheet = pd.DataFrame({ 'Period': [], 'Jam': [], 'Btw Jam': [],
                                    'Team': [], 'Skater #': [], 'Pos': [],
                                    'In': [], 'Stand': [], 'Done': [],
                                    'SEJ_1': [], 'SEJ_2': [], 'SEJ_3': []})

        # For compute In/Stand/Done
        self.nb_player_in_pb = 0
        self.nb_blocker = 0

        self.first_jam_time = -1
        self.additional_time = 0

    def __str__(self):
        return str(self.sheet)

    def get_nb_pb(self):
        return self.nb_player_in_pb

    def get_nb_blocker(self):
        return self.nb_blocker

    def is_jammer_in_pb(self):
        return (self.nb_player_in_pb - self.nb_blocker) == 1

    def new_jam(self, jam_duration):

        self.next_jam()

        if self.nb_player_in_pb > 0:
            self.additional_time += jam_duration

    def liberate_player_in_pb(self):

        """ Liberate a player in Penalty Box """

        self.nb_player_in_pb = max(0, self.nb_player_in_pb - 1)

        if self.nb_player_in_pb == 0:
            self.first_jam_time = -1
            self.additional_time = 0

    def jammer_liberate_jammer(self, id_row):

        """ Jammer is liberated by the other Jammer """

        # Modifies line
        t_done = self.additional_time + JAM_TIMER.get_time() - self.first_jam_time
        self.sheet.at[id_row, 'Stand'] = min(t_done, self.sheet.loc[id_row, 'Stand'])
        self.sheet.at[id_row, 'Done'] = t_done

        # Liberate jammer
        self.liberate_player_in_pb()

    def new_player_in_pb(self, player: Player, btw_jam, nb_penalties=1):

        """ A player arrive in Penalty Box """

        player_id = player.get_number()
        player_po = player.get_jamPosition()

        player_jp = ''
        if len(player_po) > 0:
            player_jp = player_po[0]

        if not player.is_jammer():
            self.nb_blocker += 1

        return self.write_sheet(player_id, player_jp, btw_jam, nb_penalties)

    def write_sheet(self, player_id, player_po, btw_jam, nb_penalties):

        # Compute inbox
        delay_first = 0
        if self.nb_player_in_pb == 0:
            self.first_jam_time = JAM_TIMER.get_time()
        else:
            delay_first = self.additional_time + JAM_TIMER.get_time() - self.first_jam_time

        inbox = delay_first
        done = inbox + nb_penalties * PENALTY_DURATION
        stand = done - (PENALTY_DURATION // 3)

        # Add penalty in the sheet
        row = pd.DataFrame([
            [self.period, self.jam, btw_jam, self.team, player_id, player_po, inbox, stand, done, '', '', '']],
            columns=self.col, index=[len(self.sheet)])

        self.sheet = self.sheet.append(row)
        self.nb_player_in_pb += 1

        return len(self.sheet) - 1

    def add_end_jam(self, id_row, id_sej=1):

        """ Write in the sheet that the Player in PB stay after the end of the Jam """

        id_sej = min(id_sej, 3)

        col = 'SEJ_' + str(id_sej)
        self.sheet.at[id_row, col] = (self.additional_time + JAM_TIMER.get_time() - self.first_jam_time)

    def print(self):

        self.print_sheet('PBT')


