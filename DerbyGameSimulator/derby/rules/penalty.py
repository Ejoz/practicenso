import random


class Penalty(object):

    def __init__(self, id: str, name: str, description: list):
        self.id = id
        self.name = name
        self.description = description

    def __str__(self):
        return self.name

    def get_description(self):
        id_description = random.randint(0, len(self.description) - 1)
        return self.description[id_description]

    def get_name(self):
        return self.name

    def get_id(self):
        return self.id






