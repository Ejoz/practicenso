from optparse import OptionParser

from derby.simulator.game_simulator import game_simulator

# TODO : 2 locks pour PBT - Tester bug PB pour 2 Jammers - Add NSO : Penalty Tracker

if __name__ == "__main__":

    # Default
    team_0 = 'Team/team_0.csv'
    team_1 = 'Team/team_1.csv'
    sim_time = 120 * 3
    practice = {'SK': False, 'PBT': False}

    parser = OptionParser()

    parser.add_option("--ft_0", dest="team_0", help="Input file for team 0")
    parser.add_option("--ft_1", dest="team_1", help="Input file for team 1")
    parser.add_option("-t", "--time_period", type="int", dest='time_simu', help="period duration")
    parser.add_option("--sk", action="store_true", dest='sk',
                      help="underline relevant information for ScoreKeeper")
    parser.add_option("--pbt", action="store_true", dest='pbt',
                      help="underline relevant information for Penalty Box Timer")

    (options, args) = parser.parse_args()

    # Team 0 file
    if options.team_0:
        team_0 = options.team_0

    # Team 1 file
    if options.team_1:
        team_1 = options.team_1

    # Simulation time
    if options.time_simu:
        sim_time = options.time_simu

    # Scorekeeper
    if options.sk:
        practice['SK'] = True

    # Penalty Box Manager
    if options.pbt:
        practice['PBT'] = True

    game_simulator(team_0, team_1, sim_time, practice)
