import sys
from termcolor import colored
from threading import RLock

from derby.object.timer import Timer
from derby.rules.penalty import Penalty


# ============================================================================================
#                                   SIMULATOR PARAMETERS
# ============================================================================================
DEBUG = False

SECOND_DURATION = 0.99  # Simulation speed

LANGUAGE = 'FR'

COLOR_TEAM_0 = 'blue'    # Color team 0
COLOR_TEAM_1 = 'yellow'  # Color team 1

PRACTICE_CONF = {'PBT': True, 'SK': False}

# ============================================================================================
#                                   DERBY RULES
# ============================================================================================

# TRACK POSITION
JAMMER_STR = 'Jammer'
PIVOT_STR = 'Pivot'
BLOCKER_STR = 'Blocker'
TRACK_POSITION = {JAMMER_STR: 1, PIVOT_STR: 1, BLOCKER_STR: 3}

# TIMER RULES
JAM_MAX_DURATION = 120  # Official Jam duration is 2 minutes
INTERJAM_DURATION = 5   # Non official
PENALTY_DURATION = 30   # Official time spend in Penalty Box for 1 penalty

NB_MAX_PENALTIES = 2

# PENALTY
NB_PENALTIES_FULL_OUT = 7   # A Player is full out after 7 penalties

PENALTIES_FR = [
    Penalty('G', 'Misconduct', ["rentre dans l'OPR", "n'ecoute pas l'IPR"]),
    Penalty('X', 'Cut', ["coupe la piste"]),
    Penalty('M', 'Multiplayer', ["bloque a plusieurs"]),
    Penalty('F', 'Forarm', ["bloque avec sa main"]),
    Penalty('H', 'Head Block', ["impacte avec sa tete"]),
    Penalty('L', 'Low Block', ["fais un croche pate !"])
]

PENALTIES = PENALTIES_FR
if LANGUAGE == 'FR':
    PENALTIES = PENALTIES_FR


# ============================================================================================
#                                   DERBY SHARING OBJECTS
# ============================================================================================

# Derby Object: Jam Timer
JAM_TIMER = Timer(JAM_MAX_DURATION)

# Lock penalty box
lock_pbt = RLock()
lock_pbm = RLock()


# ============================================================================================
#                                   COMMENTS
# ============================================================================================
COMMENTS_FR = ['est trop badass', 'envoie du steak', 'donne tout', 'fatigue']
COMMENTS_B_FR = ['bloque comme une malade', 'hitte sa jameuse ', 'ne laisse rien passer']
COMMENTS_J_FR = ['ratte son star pass', "n'arrive pas a passer"]

COMMENTS_EN = ['is badass']
COMMENTS_B_EN = ['hit the jammer ']
COMMENTS_J_EN = ['miss the star pass']

if LANGUAGE == 'EN':
    COMMENTS_J = COMMENTS_EN + COMMENTS_J_EN
    COMMENTS_B = COMMENTS_EN + COMMENTS_B_EN
else:
    COMMENTS_J = COMMENTS_FR + COMMENTS_J_FR
    COMMENTS_B = COMMENTS_FR + COMMENTS_B_FR

# ============================================================================================
#                                   WRITE MESSAGE
# ============================================================================================

lock_stdout = RLock()


def write_terminal(message: str, color_txt='grey', reverse=False):

    """ Write message """

    with lock_stdout:
        sys.stdout.write('\n')
        if reverse:
            sys.stdout.write(colored(message, color_txt, attrs=['reverse']))
        else:
            sys.stdout.write(colored(message, color_txt))
        sys.stdout.flush()


def whistle(message):

    """ Official whistle """

    write_terminal(message, 'magenta', True)


