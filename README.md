# PracticeNSO

PractriceNSO est un simulateur python de match de Roller Derby qui permet aux arbitres sans patins (NSO) de s’entraîner. 


## Package requis

* Termcolor
> pip install termcolor

* Pandas
> pip install pandas

## Execution
> python3 DerbyGameSimulator/run.py
